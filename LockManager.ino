#include <Arduino.h>
#include <SoftwareSerial.h>
//#include <QueueList.h>

#define ENGINE_DIR_PIN 2
#define ENGINE_STEP_PIN 3
#define WIFI_RXD 8
#define WIFI_TXD 9
#define BT_RXD 6
#define BT_TXD 7
#define BAUD 9600
#define DEBUG false

//SoftwareSerial wifiSerial(WIFI_RXD, WIFI_TXD);
SoftwareSerial bluetoothSerial(BT_RXD, BT_TXD);

//bool hasIPAddress(uint32_t);
//void displayIPOnSerialMonitor();
//int sendUDPCommandToWifi(String, const int, bool);
//int connectWithWiFi(bool);

void setup() {
  if (DEBUG) {
    Serial.begin(BAUD);
    while (!Serial);
  }
//  wifiSerial.begin(BAUD);
  pinMode(ENGINE_DIR_PIN, OUTPUT);
  pinMode(ENGINE_STEP_PIN, OUTPUT);
  btInit();
  if (DEBUG) Serial.println("BT WORKS!");
}

void loop() {
  while (bluetoothSerial.available() > 0) {
    if (bluetoothSerial.find((char *) "TMR")) {
      if (DEBUG) Serial.println("TURN RIGHT");
      turnEngineClockwise();
      delay(1000);
    } else if (bluetoothSerial.find((char *) "TML")) {
      if (DEBUG) Serial.println("TURN LEFT");
      turnEngineCounterlockwise();
      delay(1000);
    }
  }
}

void turnEngineClockwise() {
  digitalWrite(ENGINE_DIR_PIN, HIGH);
  for (int x = 0; x < 300; x++) {
    digitalWrite(ENGINE_STEP_PIN, HIGH);
    delay(10);
    digitalWrite(ENGINE_STEP_PIN, LOW);
    delay(10);
  }
}

void turnEngineCounterlockwise() {
  digitalWrite(ENGINE_DIR_PIN, LOW);
  for (int x = 0; x < 300; x++) {
    digitalWrite(ENGINE_STEP_PIN, HIGH);
    delay(10);
    digitalWrite(ENGINE_STEP_PIN, LOW);
    delay(10);
  }
}

void btInit() {
  pinMode(BT_RXD, INPUT);
  pinMode(BT_TXD, OUTPUT);
  bluetoothSerial.begin(BAUD);
  delay(1000);
  bluetoothSerial.println("AT+RESET");
  delay(1000);
  bluetoothSerial.println("AT+UART=9600,0,0");
  bluetoothSerial.println("AT+NAME=brelok");
  bluetoothSerial.println("AT+PSWD=0000");
  bluetoothSerial.println("AT+ROLE=0");
  delay(1000);
  while (bluetoothSerial.available()) {
    bluetoothSerial.read();
  }
}

/**
  * Checks if connected ESP8266 got IP address
  * @param samplingTime time in which Arduino checks WiFi state
  * @return boolean value of IP existence
  *//*

bool hasIPAddress(uint32_t samplingTime) {
  uint32_t samplingStart = millis();
  while (samplingStart + samplingTime > millis()) {
    while (wifiSerial.available() > 0) {
      if (wifiSerial.find((char *) "WIFI GOT IP")) {
        return true;
      } else {
        digitalWrite(wifiDiodePinNumber, LOW);
        return false;
      }
    }
  }
}

*/
/**
  * Displays ESP8266 IP address and port on computer's serial monitor
  *//*

void displayIPOnSerialMonitor() {
  IP = "";
  char ch = 0;
  while (1) {
    wifiSerial.println("AT+CIFSR");
    while (wifiSerial.available() > 0) {
      if (wifiSerial.find((char *) "STAIP,")) {
        delay(1000);
        Serial.print("IP Address:");
        while (wifiSerial.available() > 0) {
          ch = wifiSerial.read();
          if (ch == '+') break;
          IP += ch;
        }
      }
      if (ch == '+') break;
    }
    if (ch == '+') break;
    delay(1000);
  }
  Serial.print(IP);
  Serial.print("Port:");
  Serial.println(port);
  delay(1000);
}

*/
/**
  * Sends an UDP message to ESP8266 in order to control this module. Sends the command 5 times.
  * @param command data desired to send to ESP
  * @param timeout time given to ESP to retrieve answer
  * @param debug print to Serial window?(true = yes, false = no)
  * @return 0 if everything is fine, 1 when after 5 attempts ESP gives nothing
  *//*

int sendUDPCommandToWifi(String command, const int timeout, bool debug) {
  int sendingAttempts = 0;
  if (debug) {
    Serial.println(command);
  }
  while (sendingAttempts <= 5) {
    wifiSerial.println(command);
    while (wifiSerial.available() > 0) {
      if (wifiSerial.find((char *) "OK")) {
        sendingAttempts = 7;
      }
    }
    delay(timeout);
    sendingAttempts++;
  }
  if (sendingAttempts == 8) {
    if (debug) {
      Serial.println("OK");
    }
    delay(1000);
    return 0;
  } else {
    if (debug) {
      Serial.println("Error");
    }
    delay(1000);
    return 1;
  }
}

*/
/**
  * Connects ESP8266 with WiFi
  * @param debug print to Serial window?(true = yes, false = no)
  * @return 0 when connected, 1 when not
  *//*

int connectWithWiFi(bool debug) {
  wifiSerial.begin(BAUD);
  if (sendUDPCommandToWifi("AT", 1000, DEBUG)) {
    if (debug) {
      Serial.println("Wifi probably turned off, aborting");
    }
    return 1;
  }
  sendUDPCommandToWifi("AT+RST", 5000, DEBUG);
  sendUDPCommandToWifi("AT+CWQAP", 1000, DEBUG);
  sendUDPCommandToWifi("AT+CWMODE=1", 1000, DEBUG);
  hasIP = hasIPAddress(5000);
  if (!hasIP) {
    Serial.println("Connecting WiFi....");
    if (!sendUDPCommandToWifi("AT+CWJAP=\"EngineerThesisTest\",\"TestPassword\"", 7000, DEBUG)) {
      if (debug) {
        Serial.println("WiFi Connected");
      }
    } else {
      if (debug) {
        Serial.println("WiFi probably turned off, aborting");
      }
      wifiSerial.end();
      return 1;
    }
  }
  digitalWrite(wifiDiodePinNumber, HIGH);
  if (debug) {
    displayIPOnSerialMonitor();
  }
  delay(2000);
  sendUDPCommandToWifi("AT+CIPMUX=1", 100, DEBUG);
  sendUDPCommandToWifi("AT+CIPSERVER=1," + port, 100, DEBUG);
  return 0;
}*/
